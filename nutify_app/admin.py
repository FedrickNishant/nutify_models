from django.contrib import admin
from .models import (Company,CompanyCertificationImages,Individual,
                    Supplier,SupplierAnalytics,CompanyIndividualMembership,
                    Task,TaskMembership,Search,Leads,Message,Notifications)
# Register your models here.

class CompanyCertificationImagesInline(admin.TabularInline):
    model = CompanyCertificationImages
    extra = 3

class CompanyAdmin(admin.ModelAdmin):
    inlines = [ CompanyCertificationImagesInline]

admin.site.register(Company,CompanyAdmin)
admin.site.register(Individual)
admin.site.register(Supplier)
admin.site.register(SupplierAnalytics)
admin.site.register(CompanyIndividualMembership)
admin.site.register(Task)
admin.site.register(TaskMembership)
admin.site.register(Search)
admin.site.register(Leads)
admin.site.register(Message)
admin.site.register(Notifications)