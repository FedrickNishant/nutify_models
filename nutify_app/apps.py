from django.apps import AppConfig


class NutifyAppConfig(AppConfig):
    name = 'nutify_app'
