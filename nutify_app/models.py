from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
# Create your models here.

UNDEFINED = 'UD'
class Company(models.Model):

    class Meta:
        verbose_name_plural = _("Companies")

    user = models.OneToOneField(User,on_delete=models.DO_NOTHING, primary_key=True)
    address = models.CharField(_("Address"), max_length=1000)
    about = models.CharField(_("About"), max_length=1000)
    date_of_establishment = models.DateTimeField(_("Date of Establishment"))
    location = models.CharField(_("Location"), max_length=100)
    
    AREAS_NUTRA = [
        ('AREA1', 'AREA 1'),
        ('AREA2', 'AREA 2'),
        ('AREA3', 'AREA 3'),
        ('AREA4', 'AREA 4'),
    ]
    TYPES_OF_MARKET = [
        ('TYPE1', 'TYPE 1'),
        ('TYPE2', 'TYPE 2'),
        ('TYPE3', 'TYPE 3'),
        ('TYPE4', 'TYPE 4'),
    ]
    PLANS = [
        ('PLAN1', 'PLAN 1'),
        ('PLAN2', 'PLAN 2'),
        ('PLAN3', 'PLAN 3'),
        ('PLAN4', 'PLAN 4'),
    ]
    areas_to_focus_in_nutra = models.CharField(_("Areas to focus in nutra"), max_length=100,choices=AREAS_NUTRA, default=UNDEFINED)
    types_of_market = models.CharField(_("Types of market access"), max_length=100,choices=TYPES_OF_MARKET, default=UNDEFINED)
    subscription_plan = models.CharField(_("Subscription plan"), max_length=100,choices=PLANS, default=UNDEFINED)

    def __str__(self):
        return str(self.user)



class CompanyCertificationImages(models.Model):
    class Meta:
        verbose_name_plural = _("Company Certification Images")

    company = models.ForeignKey(Company,on_delete=models.CASCADE)
    company_certificate = models.ImageField(_("Company Certificate"),upload_to ="company_certificate")

    def __str__(self):
        return str(self.company)


class Individual(models.Model):
    class Meta:
        verbose_name_plural = _("Individuals")

    user = models.OneToOneField(User,on_delete=models.DO_NOTHING, primary_key=True)
    country = models.CharField(_("Country"), max_length=100)
    city = models.CharField(_("City"), max_length=100)
    recent_job_title = models.CharField(_("Recent Job Title"), max_length=100)
    
    EMPLOYEMENT_TYPE = [
        ('TYPE1', 'TYPE 1'),
        ('TYPE2', 'TYPE 2'),
        ('TYPE3', 'TYPE 3'),
        ('TYPE4', 'TYPE 4'),
    ]
    INDUSTRY = [
        ('INDUSTRY1', 'INDUSTRY 1'),
        ('INDUSTRY2', 'INDUSTRY 2'),
        ('INDUSTRY3', 'INDUSTRY 3'),
        ('INDUSTRY4', 'INDUSTRY 4'),
    ]
    employment_type = models.CharField(_("Employment Type"), max_length=100,choices=EMPLOYEMENT_TYPE, default=UNDEFINED)
    industry = models.CharField(_("Industry"), max_length=100,choices=INDUSTRY, default=UNDEFINED)
    profile_image = models.ImageField(_("Profile Image"),upload_to ="individual_profile_image")


    def __str__(self):
        return str(self.user)


class Supplier(models.Model):
    class Meta:
        verbose_name_plural = _("Suppliers")

    user = models.OneToOneField(User,on_delete=models.DO_NOTHING, primary_key=True)
    description = models.CharField(_("Description"), max_length=1000)
    
    CATEGORY_CHOICES = [
        ('CATEGORY1', 'CATEGORY 1'),
        ('CATEGORY2', 'CATEGORY 2'),
        ('CATEGORY3', 'CATEGORY 3'),
        ('CATEGORY4', 'CATEGORY 4')
    ]
    PLANS = [
        ('PLAN1', 'PLAN 1'),
        ('PLAN2', 'PLAN 2'),
        ('PLAN3', 'PLAN 3'),
        ('PLAN4', 'PLAN 4'),
    ]
    category = models.CharField(_("Category"), max_length=100,choices=CATEGORY_CHOICES, default=UNDEFINED)
    address = models.CharField(_("Address"), max_length=1000)
    lab_equipments = models.CharField(_("Lab equipments"), max_length=1000)
    f_d_services = models.CharField(_("F & D Services"), max_length=1000)
    factory_equipments = models.CharField(_("Factory equipments"), max_length=1000)
    formats_of_dosage = models.CharField(_("Formats of dosage"), max_length=1000)
    capacity_of_each_facility = models.CharField(_("Capacity of each facility"), max_length=1000)
    factory_certificate_image = models.ImageField(_("Factory certificate image"),upload_to ="supplier_media")
    lab_certificate_image = models.ImageField(_("Lab certificate image"),upload_to ="supplier_media")
    is_certified_factory = models.BooleanField(_("Is certified factory?"))
    has_certified_labs = models.BooleanField(_("Has certified labs?"))
    subscription_plan = models.CharField(_("Subscription plan"), max_length=100,choices=PLANS, default=UNDEFINED)

    def __str__(self):
        return str(self.user)


class SupplierAnalytics(models.Model):
    class Meta:
        verbose_name_plural = _("Supplier Analytics")

    supplier = models.ForeignKey(Supplier,on_delete=models.CASCADE)
    timestamp = models.DateTimeField(_("Timestamp"))
    shortlisted_count = models.IntegerField(_("Shortlisted count"))
    bookmarked_count = models.IntegerField(_("Bookmarked count"))

    def __str__(self):
        return f"{self.supplier}"


class CompanyIndividualMembership(models.Model):
    class Meta:
        verbose_name_plural = _("Company Individual Memberships")

    company = models.ForeignKey(Company,on_delete=models.CASCADE)
    individual = models.ForeignKey(Individual,on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.company} | {self.individual}"



class Task(models.Model):
    class Meta:
        verbose_name_plural = _("Tasks")

    company = models.ForeignKey(Company,on_delete=models.CASCADE)
    name = models.CharField(_("Name"), max_length=1000)
    
    TYPES = [
        ('TYPE1', 'TYPE 1'),
        ('TYPE2', 'TYPE 2'),
        ('TYPE3', 'TYPE 3'),
        ('TYPE4', 'TYPE 4'),
    ]
    types = models.CharField(_("Type"), max_length=100,choices=TYPES, default=UNDEFINED)
    description = models.CharField(_("Description"), max_length=1000)
    time_to_allot = models.IntegerField(_("Time to allot for this task"))
    slots_available = models.IntegerField(_("Slots available"))
    is_open = models.BooleanField(_("Is open?"))
    is_closed = models.BooleanField(_("Is closed?"))
    is_paused = models.BooleanField(_("Is paused?"))

    def __str__(self):
        return f"{self.company} | {self.name}"


class TaskMembership(models.Model):
    class Meta:
        verbose_name_plural = _("Task Memberships")

    company_individual_membership= models.ForeignKey(CompanyIndividualMembership,on_delete=models.CASCADE)
    is_approved = models.BooleanField(_("Is approved?"))
    is_rejected = models.BooleanField(_("Is rejected?"))

    def __str__(self):
        return f"{self.company_individual_membership}"


class Search(models.Model):
    class Meta:
        verbose_name_plural = _("Searches")

    task_membership = models.ForeignKey(TaskMembership,on_delete=models.CASCADE)
    is_saved_search = models.BooleanField(_("Is saved search?"))

    def __str__(self):
        return f"{self.task_membership}"

class Leads(models.Model):
    class Meta:
        verbose_name_plural = _("Leads")

    task_membership = models.ForeignKey(TaskMembership,on_delete=models.CASCADE)
    supplier = models.ForeignKey(Supplier,on_delete=models.CASCADE)
    is_connected = models.BooleanField(_("Is connected?"))
    is_bookmarked = models.BooleanField(_("Is bookmarked?"))
    is_shortlisted = models.BooleanField(_("Is shortlisted?"))
    is_blocked_by_individual = models.BooleanField(_("Is blocked by Individual?"))
    is_blocked_by_supplier = models.BooleanField(_("Is blocked by supplier?"))
    is_closed = models.BooleanField(_("Is closed?"))
    is_approved_by_supplier = models.BooleanField(_("Is approved by supplier?"))
    is_rejected_by_supplier = models.BooleanField(_("Is rejected by supplier?"))
    
    def __str__(self):
        return f"{self.task_membership}"


class Message(models.Model):
    class Meta:
        verbose_name_plural = _("Messages")
        
    leads = models.ForeignKey(Leads,on_delete=models.CASCADE)
    sent_by = models.ForeignKey(User ,on_delete= models.CASCADE)
    timestamp = models.DateTimeField(_("Timestamp"))

    def __str__(self):
        return f"{self.leads} | {self.sent_by}"


class Notifications(models.Model):
    class Meta:
        verbose_name_plural = _("Notifications")

    user = models.ForeignKey(User,on_delete=models.CASCADE)
    message = models.CharField(_("Message"),max_length=100)
    is_read = models.BooleanField(_("Is read?"))

    def __str__(self):
        return f"{self.user} | {self.message}"
